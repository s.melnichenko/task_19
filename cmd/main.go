package main

import "fmt"

func main() {
	Fn()
}

func Fn() {
	var (
		countLeapYear         int    // number of leap years.
		countDayInLeapYear    = 366  // number of days in a leap year.
		countDayInNonLeapYear = 365  // number of days in a non-leap year.
		countYear             = 100  // total number of years.
		startYear             = 1900 // first year of reference.
	)

	// calculate the number of high years from January 1, 1990 to December 31, 2000.
	if (startYear+99)%400 == 0 {
		countLeapYear = countYear / 4
	} else {
		countLeapYear = countYear/4 - 1
	}

	// calculate the number of days from January 1, 1900 to December 31, 2000.
	countDay := (countLeapYear * countDayInLeapYear) + ((countYear - countLeapYear) * countDayInNonLeapYear)

	// calculate the number of Sundays from January 1, 1900 to December 31, 2000.
	result := countDay / 7

	fmt.Println(result)
}
